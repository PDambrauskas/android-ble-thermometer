package lt.xz.tinklas.blecontroller;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanSettings;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ListView;

import lt.xz.tinklas.blecontroller.adapters.BluetoothLeDeviceListAdapter;
import lt.xz.tinklas.blecontroller.callbacks.BluetoothLeScanCallback;
import lt.xz.tinklas.blecontroller.handlers.BluetoothLeDeviceDiscoveryHandler;
import lt.xz.tinklas.blecontroller.handlers.BluetoothLeDeviceHandler;
import lt.xz.tinklas.blecontroller.listeners.DeviceOnClickListener;


public class DeviceActivity extends BaseActivity {

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBluetoothLeScanner;
    private ScanCallback mScanCallback;
    private Handler mHandler;
    private BluetoothLeDeviceListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_thermometer);

        ListView deviceList = (ListView) findViewById(R.id.devices);
        mAdapter = new BluetoothLeDeviceListAdapter(this);
        deviceList.setAdapter(mAdapter);
        Handler deviceHandler = new BluetoothLeDeviceHandler(this);
        deviceList.setOnItemClickListener(new DeviceOnClickListener(deviceHandler, this));

        BluetoothManager manager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        mBluetoothAdapter = manager.getAdapter();
        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        mHandler = new BluetoothLeDeviceDiscoveryHandler(mAdapter);
        mScanCallback = new BluetoothLeScanCallback(mHandler);
        startScan();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.clear();
        startScan();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopScan();
    }

    private void startScan() {
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            showBluetoothModal();
            finish();
        }
        else {
            ScanSettings settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            mBluetoothLeScanner.startScan(null, settings, mScanCallback);
        }
    }

    private void stopScan() {
        mBluetoothLeScanner.stopScan(mScanCallback);
    }

    private void showBluetoothModal() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivity(enableBtIntent);
    }
}
