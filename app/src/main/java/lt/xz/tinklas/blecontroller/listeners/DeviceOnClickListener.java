package lt.xz.tinklas.blecontroller.listeners;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;

import lt.xz.tinklas.blecontroller.utils.Constants;

import lt.xz.tinklas.blecontroller.callbacks.BluetoothLeConnectCallback;
import lt.xz.tinklas.blecontroller.model.BluetoothLeDevice;
import lt.xz.tinklas.blecontroller.utils.Preferences;

public class DeviceOnClickListener implements AdapterView.OnItemClickListener {
    private Handler mDeviceHandler;
    private Context context;

    public DeviceOnClickListener(Handler deviceHandler, Context context) {
        this.mDeviceHandler = deviceHandler;
        this.context = context;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        BluetoothLeDevice device = (BluetoothLeDevice) parent.getAdapter().getItem(position);
        Context context = parent.getContext();
        device.getDevice()
                .connectGatt(context, false, new BluetoothLeConnectCallback(mDeviceHandler));
        saveDeviceAddress(device);
        ((Activity)context).finish();
    }

    private void saveDeviceAddress(BluetoothLeDevice device) {
        Preferences.getInstance(context)
            .storePreference(Constants.BLE_PREFS_DEVICE, device.getAddress());
    }
}
