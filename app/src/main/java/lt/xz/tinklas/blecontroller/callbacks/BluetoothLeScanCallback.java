package lt.xz.tinklas.blecontroller.callbacks;

import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.os.Handler;
import android.os.Message;

import java.util.List;

import lt.xz.tinklas.blecontroller.model.BluetoothLeDevice;

public class BluetoothLeScanCallback extends ScanCallback {
    private Handler mHandler;

    public BluetoothLeScanCallback(Handler handler) {
        mHandler = handler;
    }

    @Override
    public void onScanResult(int callbackType, ScanResult result) {
        processResult(result);
    }

    @Override
    public void onBatchScanResults(List<ScanResult> results) {
        for (ScanResult result : results) {
            processResult(result);
        }
        mHandler.sendMessage(Message.obtain(null, -1));
    }

    private void processResult(ScanResult result) {
        BluetoothLeDevice beacon = new BluetoothLeDevice(result.getScanRecord(),
                result.getDevice(),
                result.getRssi());
        mHandler.sendMessage(Message.obtain(null, 0, beacon));
    }
}
