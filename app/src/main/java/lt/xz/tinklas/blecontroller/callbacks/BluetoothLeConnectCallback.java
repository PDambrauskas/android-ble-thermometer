package lt.xz.tinklas.blecontroller.callbacks;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.os.Handler;
import android.os.Message;

import java.util.UUID;

import lt.xz.tinklas.blecontroller.utils.Constants;

public class BluetoothLeConnectCallback extends BluetoothGattCallback {
    private Handler mHandler;

    public BluetoothLeConnectCallback(Handler handler) {
        mHandler = handler;
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt,
                                        final BluetoothGattCharacteristic characteristic) {
        broadcastUpdate(Constants.ACTION_UPDATE_TEMPERATURE, characteristic.getStringValue(0));
        gatt.disconnect();
        gatt.close();
    }

    @Override
    public void onConnectionStateChange(final BluetoothGatt gatt, final int status,
                                        final int newState) {
        if (newState == BluetoothProfile.STATE_CONNECTED) {
            broadcastUpdate(Constants.ACTION_GATT_CONNECTED, null);
            gatt.discoverServices();
        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            broadcastUpdate(Constants.ACTION_GATT_DISCONNECTED, null);
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        BluetoothGattService service =
                gatt.getService(UUID.fromString(Constants.THERMOMETER_UUID));
        BluetoothGattCharacteristic characteristic =
                service.getCharacteristic(UUID.fromString(Constants.TEMPERATURE_CHARACTERISTIC_UUID));
        gatt.setCharacteristicNotification(characteristic, true);
    }

    private void broadcastUpdate(int actionId, Object message) {
        mHandler.sendMessage(Message.obtain(null, actionId, message));
    }
}
