package lt.xz.tinklas.blecontroller;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;


public class BaseActivity extends ActionBarActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_devices) {
            Intent deviceIntent = new Intent(this, DeviceActivity.class);
            this.startActivity(deviceIntent);
        }

        return super.onOptionsItemSelected(item);
    }
}
