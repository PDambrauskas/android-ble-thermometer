package lt.xz.tinklas.blecontroller.listeners;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import lt.xz.tinklas.blecontroller.MainActivity;
import lt.xz.tinklas.blecontroller.TemperatureActivity;

public class TemperatureReceiver extends BroadcastReceiver {
    private TemperatureActivity temperatureActivity;

    public TemperatureReceiver(TemperatureActivity temperatureActivity) {
        this.temperatureActivity = temperatureActivity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String currentTemperature = intent.getStringExtra(MainActivity.TEMPERATURE_KEY);
        temperatureActivity.updateTemperature(currentTemperature);
    }
}
