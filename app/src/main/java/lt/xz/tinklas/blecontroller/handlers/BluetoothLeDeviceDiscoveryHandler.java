package lt.xz.tinklas.blecontroller.handlers;

import android.os.Handler;
import android.os.Message;
import android.widget.ArrayAdapter;

public class BluetoothLeDeviceDiscoveryHandler extends Handler {
    private ArrayAdapter deviceListAdapter;

    public BluetoothLeDeviceDiscoveryHandler(ArrayAdapter deviceListAdapter) {
        this.deviceListAdapter = deviceListAdapter;
    }

    @Override
    public void handleMessage(Message msg) {
          deviceListAdapter.setNotifyOnChange(false);
          deviceListAdapter.add(msg.obj);
          deviceListAdapter.notifyDataSetChanged();
    }
}
