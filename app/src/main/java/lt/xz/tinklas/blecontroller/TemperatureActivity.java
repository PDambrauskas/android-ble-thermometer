package lt.xz.tinklas.blecontroller;

public interface TemperatureActivity {
    public void updateTemperature(String temperature);
}
