package lt.xz.tinklas.blecontroller.utils;

public class Constants {
    public static final int ACTION_GATT_CONNECTED = 1;
    public static final int ACTION_GATT_DISCONNECTED = 0;
    public static final int ACTION_UPDATE_TEMPERATURE = 2;

    public static final String THERMOMETER_UUID = "0000ffe0-0000-1000-8000-00805f9b34fb";
    public static final String TEMPERATURE_CHARACTERISTIC_UUID = "0000ffe1-0000-1000-8000-00805f9b34fb";
    public static final String BLE_PREFS_KEY = "lt.xz.tinklas.blecontroller";
    public static final String BLE_PREFS_DEVICE = "DEVICE";
}
