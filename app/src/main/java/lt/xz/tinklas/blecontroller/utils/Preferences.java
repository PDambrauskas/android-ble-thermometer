package lt.xz.tinklas.blecontroller.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class Preferences {
    private static Preferences preferences;
    private SharedPreferences settings;

    public static Preferences getInstance(Context context) {
        if (preferences == null)
            preferences = new Preferences(context);
        return preferences;
    }

    private Preferences(Context context) {
        this.settings = context.getSharedPreferences(Constants.BLE_PREFS_KEY, context.MODE_PRIVATE);
    }

    public void storePreference(String key, String val) {
        settings.edit().putString(key, val).commit();
    }

    public String getString(String key) {
        return settings.getString(key, null);
    }
}
