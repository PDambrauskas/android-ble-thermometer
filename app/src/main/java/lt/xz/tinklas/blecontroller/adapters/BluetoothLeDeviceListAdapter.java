package lt.xz.tinklas.blecontroller.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import lt.xz.tinklas.blecontroller.R;
import lt.xz.tinklas.blecontroller.model.BluetoothLeDevice;

public class BluetoothLeDeviceListAdapter extends ArrayAdapter<BluetoothLeDevice> {
    public BluetoothLeDeviceListAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.item_device, parent, false);
        }

        BluetoothLeDevice beacon = getItem(position);
        TextView nameView = (TextView) convertView.findViewById(R.id.device_name);
        nameView.setText(beacon.getRecord().getDeviceName());
        TextView addressView = (TextView) convertView.findViewById(R.id.device_address);
        addressView.setText(beacon.getAddress());
        return convertView;
    }
}
