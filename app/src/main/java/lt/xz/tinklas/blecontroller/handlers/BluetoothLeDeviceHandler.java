package lt.xz.tinklas.blecontroller.handlers;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import lt.xz.tinklas.blecontroller.MainActivity;
import lt.xz.tinklas.blecontroller.R;
import lt.xz.tinklas.blecontroller.utils.Constants;

public class BluetoothLeDeviceHandler extends Handler {
    private Context context;

    public BluetoothLeDeviceHandler(Context context) {
        this.context = context;
    }

    @Override
    public void handleMessage(Message msg) {
        Toast toast;
        switch (msg.what) {
            case Constants.ACTION_GATT_CONNECTED:
                toast = Toast.makeText(context,
                        context.getString(R.string.device_connected),
                        Toast.LENGTH_SHORT);
                toast.show();
                break;
            case Constants.ACTION_GATT_DISCONNECTED:
                toast = Toast.makeText(context,
                        context.getString(R.string.device_disconnected),
                        Toast.LENGTH_SHORT);
                toast.show();
                break;
            case Constants.ACTION_UPDATE_TEMPERATURE:
                Intent intent = new Intent(MainActivity.TEMPERATURE_KEY);
                intent.putExtra(MainActivity.TEMPERATURE_KEY, msg.obj.toString());
                context.sendBroadcast(intent);
        }
    }
}
