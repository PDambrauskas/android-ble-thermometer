package lt.xz.tinklas.blecontroller;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.TextView;
import android.widget.Toast;

import lt.xz.tinklas.blecontroller.callbacks.BluetoothLeConnectCallback;
import lt.xz.tinklas.blecontroller.handlers.BluetoothLeDeviceHandler;
import lt.xz.tinklas.blecontroller.listeners.TemperatureReceiver;
import lt.xz.tinklas.blecontroller.utils.Constants;
import lt.xz.tinklas.blecontroller.utils.Preferences;

public class MainActivity extends BaseActivity implements TemperatureActivity, SwipeRefreshLayout.OnRefreshListener {

    public static final String TEMPERATURE_KEY = "MainActivity.temperature";
    private BroadcastReceiver mReceiver;
    private TextView mTemperatureText;
    private SwipeRefreshLayout mSwipeLayout;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeDeviceHandler mDeviceHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTemperatureText = (TextView) findViewById(R.id.temperature);
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.main_container);
        mSwipeLayout.setOnRefreshListener(this);

        mSwipeLayout.setColorSchemeResources(android.R.color.holo_blue_dark);
        BluetoothManager manager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        mBluetoothAdapter = manager.getAdapter();
        mDeviceHandler = new BluetoothLeDeviceHandler(this);

        updateTemperature("N/A");
        initializeReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeReceiver();
    }

    @Override
    public void onRefresh() {
        String deviceAddress = getDeviceAdr();
        if (deviceAddress != null) {
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(deviceAddress);
            device.connectGatt(this, true, new BluetoothLeConnectCallback(mDeviceHandler));
        }
        else {
            Toast.makeText(this, "Please select thermometer.", Toast.LENGTH_SHORT).show();
            Intent deviceIntent = new Intent(this, DeviceActivity.class);
            this.startActivity(deviceIntent);
        }
        mSwipeLayout.setRefreshing(false);
    }

    @Override
    public void updateTemperature(String temperature) {
        mTemperatureText.setText(
                String.format(getResources().getString(R.string.default_temperature),temperature)
        );
    }

    private void initializeReceiver() {
        if (mReceiver == null) {
            mReceiver = new TemperatureReceiver(this);
            this.registerReceiver(mReceiver, new IntentFilter(TEMPERATURE_KEY));
        }
    }

    private String getDeviceAdr() {
       return Preferences.getInstance(this).getString(Constants.BLE_PREFS_DEVICE);
    }

}
