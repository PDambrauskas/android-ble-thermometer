package lt.xz.tinklas.blecontroller.model;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanRecord;

public class BluetoothLeDevice {
    private ScanRecord record;
    private BluetoothDevice device;
    private String address;
    private int rssi;

    public BluetoothLeDevice(ScanRecord record, BluetoothDevice device, int rssi) {
        this.record = record;
        this.device = device;
        this.address = device.getAddress();
        this.rssi = rssi;
    }

    public ScanRecord getRecord() {
        return record;
    }

    public String getAddress() {
        return address;
    }

    public int getRssi() {
        return rssi;
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public void setRecord(ScanRecord record) {
        this.record = record;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }
}
