#include <SoftwareSerial.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define RxD 2
#define TxD 3

SoftwareSerial BLE(RxD,TxD);

#define ONE_WIRE_BUS 4
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
 
void setup() 
{ 
  Serial.begin(9600); 
  pinMode(RxD, INPUT);
  pinMode(TxD, OUTPUT);
  setupBleConnection();
  sensors.begin();
} 
 
void loop() 
{ 
  char rch;
  while(1) {
    String response = "";
    while (BLE.available()) {
      rch = BLE.read();
      response += rch;
    }

    if (response == "OK+CONN") {
      delay(500);
      sensors.requestTemperatures();
      int temp = sensors.getTempCByIndex(0);
      BLE.print(temp);
    }
    delay(500);
  }
} 

void setupBleConnection()
{
  BLE.begin(9600);
  BLE.print("AT+ROLE0");
  delay(500);
  BLE.print("AT+MODE0");
  delay(500);
  BLE.print("AT+PWRM0");
  delay(500);
  BLE.print("AT+NAMEthermometer");
  delay(500);
  BLE.print("AT+NOTI1");
  delay(500);
  BLE.print("AT+RESET");
  delay(800);
}
